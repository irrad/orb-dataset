from __future__ import annotations

from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By

from scrapers.peerj_config import peerj_paper_selectors, peerj_reports_selectors
from util.web_browser import BrowserWrapper
from util.web_tools import safe_key, parse_html_with_selector_config, build_xpath

import datetime
import json
import pickle
import time



URL = "https://peerj.com/articles/?type=articles&journal=cs&manuscriptType=research-article"

BASE_URL = "https://peerj.com"
DEFAULT_SUBMISSIONS_URL  = "https://peerj.com/articles/?type=articles"
SUBMISSIONS_PAGE_PATTERN = "https://peerj.com/articles/?type=articles&page="
ARTICLE_PAGE_PATTERN = "https://peerj.com/articles/"

journal = ['peerj', 'cs', 'matsci', 'pchem', 'achem', 'ochem', 'ichem']
manuscriptType = ['research-article', 'literature-review', 'bioinformatics-tool', 'systematic-review', 'registered-report-s1']


def wait_for_page_buttons(driver):
    node = '//button'
    attributes = {'class': 'v-pagination__item'}
    return wait_until_xpath(driver, build_xpath(node, attributes))

def wait_for_searchpage(driver):
    node = '//div'
    attributes = {'class': 'row mb-12 main-search-item-row mt-4',
                  'index': 14,
                 }
    return wait_until_xpath(driver, build_xpath(node, attributes))

def wait_until_xpath(driver, xpath):
    try:
        element = driver.find_element(By.XPATH, xpath)
        return True if element else False
    except:
        return False


class PeerJScraper:
    def __init__(self, use_browser='firefox', parser='lxml'):
        # using lxml by default
        # parser because html.parser was missing some <meta> tag closures
        self.parser = parser
        self.use_browser = use_browser
        
        self.last_filename = None
        self.submissions_data_list = None
    
    def build_absolute_url(self, relative_url):
        return f'{BASE_URL}{relative_url}'

    def build_submission_url_by_year(self, year):
        return f'https://peerj.com/articles/?type=articles&published-gte={year}&published-lte={year}'
    
    def extract_all_submissions(self):
        self.browser = BrowserWrapper(self.use_browser)
        self.submissions_data_list = self.get_submissions_year_by_year()
        return self.submissions_data_list

    def get_submissions_year_by_year(self):
        FIRST_SCIPOST_YEAR = 2013
        CURRENT_YEAR = int(datetime.datetime.now().year)

        submissions_list = []
        for year in range(FIRST_SCIPOST_YEAR, CURRENT_YEAR+1):
            submissions_url_for_year = self.build_submission_url_by_year(year)
            current_submissions = self.get_submissions_from_articles_webpage(submissions_url_for_year, f'{submissions_url_for_year}&page=')
            pickle.dump(current_submissions, open(f'submission_list_year_{year}.pickle', 'wb'))
            submissions_list.extend(current_submissions)
        return submissions_list

    def get_submissions_from_last_failed_pages(self, failed_pages):
        # ninety seconds should be long enough to load unspecified numer of rows
        # but still lesser than up required id==14
        self.browser.wait_time = 90

        # not specifying wait condition so the wait_time would be taken by default
        ids_list, failed_pages = self.extract_article_links_from_collection_pages(failed_pages, wait_condition=None)

        print(f'Crawled and scraped total number of {len(ids_list)} articles links')
        print(f'Saving failed pages: {failed_pages}')

        failed_pages_filename = 'failed_pages.pickle'
        self.save_failed_pages_links_to_pickle(failed_pages, failed_pages_filename)

        pickle.dump(submissions, open(f'submissions_from_last_failed_pages.pickle', 'wb'))

        submissions = self.extract_submissions_from_url_list(ids_list)
        return submissions

    def get_submissions_from_articles_webpage(self, chosen_url=DEFAULT_SUBMISSIONS_URL, submission_page_pattern=SUBMISSIONS_PAGE_PATTERN):
        no_pages = self.get_number_of_peerj_pages(chosen_url)
        links_to_crawl = self.get_page_links_to_crawl(no_pages, submission_page_pattern)
        print(len(links_to_crawl))

        ids_list, failed_pages = self.extract_article_links_from_collection_pages(links_to_crawl)

        print(f'Crawled and scraped total number of {len(ids_list)} articles links')
        print(f'Saving failed pages: {failed_pages}')

        failed_pages_filename = 'failed_pages.pickle'
        self.save_failed_pages_links_to_pickle(failed_pages, failed_pages_filename)

        return self.extract_submissions_from_url_list(ids_list)

    def extract_article_links_from_collection_pages(self, links_to_crawl, wait_condition=wait_for_searchpage):
        ids_list = []
        failed_pages = []
        #for page in links_to_crawl:
        pg_counter = 0
        exception_retries = 0
        while True:
            if pg_counter >= len(links_to_crawl):
                break
            page = links_to_crawl[pg_counter]
            try:
                new_ids = self.get_articles_from_page(page, wait_condition)
                ids_list.extend(new_ids)
                pg_counter += 1
                #no exception here, so resetting retries
                exception_retries = 0
            except Exception as e:
                print(f'Error when processing ids from page {page}', {e})
                exception_retries += 1
                # do not retry indefinitely, 3 retries are enough
                if exception_retries >= 3:
                    failed_pages.append(page)
                    pg_counter += 1
                    exception_retries = 0
                    continue
                time.sleep(90)
        return ids_list,failed_pages

    def extract_submissions_from_url_list(self, ids_list):
        submissions = []
        for article_id in ids_list:
            print(f'processing {article_id}')
            article_link = f'{ARTICLE_PAGE_PATTERN}{article_id}'

            try:
                main_submission_html = self.browser.render_html(article_link)
                main_submission_soup = BeautifulSoup(main_submission_html, self.parser)
                main_submission = self.parse_article_webpage(main_submission_soup)

                reports = None
                if main_submission['reports_url']:
                    reports_html = self.browser.render_html(f"{BASE_URL}{main_submission['reports_url']}")
                    reports_soup = BeautifulSoup(reports_html, self.parser)
                    reports = self.parse_reviews_webpage(reports_soup)

                submissions.append([main_submission, reports])

            except Exception as e:
                print(f'Error when processing article {article_id}', {e})

        print(f'Crawled and scraped total number of {len(submissions)} submissions')
        return submissions

    def get_number_of_peerj_pages(self, chosen_url=DEFAULT_SUBMISSIONS_URL):
        articles_mainpage_html = self.browser.render_html(chosen_url, wait_for_page_buttons)
        first_page_soup = BeautifulSoup(articles_mainpage_html, self.parser)
        page_buttons = first_page_soup.find_all('button', {"class":"v-pagination__item"})
        if not page_buttons:
            raise ValueError("Page buttons were not found")
            
        last_page_str = page_buttons[-1].get_text()
        if int(last_page_str) < 1:
            raise ValueError("Failed to retrieve number of pages")
        return int(last_page_str)

    def get_page_links_to_crawl(self, no_pages, submission_page_pattern=SUBMISSIONS_PAGE_PATTERN):
        list_of_links = []
        for page in range(1, no_pages+1, 1):
            current_page = f'{submission_page_pattern}{page}'
            list_of_links.append(current_page)
        if not list_of_links:
            raise ValueError("List of pages were not found")
        return list_of_links
    
    def get_articles_from_page(self, page_link, wait_condition=wait_for_searchpage):
        colective_page_html = self.browser.render_html(page_link, wait_condition)
        colective_page_soup = BeautifulSoup(colective_page_html, self.parser)
        current_page_articles = colective_page_soup.find_all('div', {"class":"main-search-item-row"})
        
        if not current_page_articles:
            raise ValueError("Articles on page were not found (class:main-search-item-row)")
    
        id_collection = []
        for article in current_page_articles:
            article_id = article.select('a')[0].get('href').split('/')[-1]
            # print(f'Id= {article_id}: {article_link}')
            id_collection.append(article_id)
            
        if not current_page_articles:
            raise ValueError("For some resons, the articles IDs were not extracted")
        return id_collection
    
    def parse_article_webpage(self, soup):
        paper_selectors = json.loads(peerj_paper_selectors)
        print(paper_selectors.keys())
        article_dict = parse_html_with_selector_config(soup, paper_selectors['submission'])
        return article_dict
    
    def parse_reviews_webpage(self, soup):
        reports_selectors = json.loads(peerj_reports_selectors)
        reports_dict = parse_html_with_selector_config(soup, reports_selectors['reports'])
        return reports_dict

    def to_pickle(self, filename=None):
        self.last_filename = filename if filename else f'{str(datetime.datetime.now())[:-7]}_peerj_submissions_data_list.pickle'
        pickle.dump(self.submissions_data_list, open(self.last_filename, 'wb'))

    def from_pickle(self, filename=None):
        chosen_file = filename if filename else self.last_filename

        if chosen_file:
            return pickle.load(open(chosen_file, 'rb'))
        else:
            return None

    def save_failed_pages_links_to_pickle(self, failed_pages, failed_pages_filename):
        try:
            failed_pages_pickle = open(failed_pages_filename, 'rb')
            content = pickle.load(failed_pages_pickle)
            failed_pages_pickle.close()
        except:
            content = []

        content.extend(failed_pages)
        failed_pages_pickle = open(failed_pages_filename, 'wb')
        pickle.dump(content, failed_pages_pickle)
        failed_pages_pickle.close()
