import json
peerj_paper_selectors = '''{
    "base_url" : "https://peerj.com",
    "submission": {
        "title" : {
            "selector" : "meta[name='citation_title']",
            "attribute" : "content",
            "index" : 0
        },
        "doi" : {
            "selector" : "meta[name='citation_doi']",
            "attribute" : "content"
        },
        "venue" : {
            "selector" : ".article-section-breadcrumb"
        },
        "reports_url" : {
            "selector" : ".view-public-reviews a",
            "attribute" : "href"
        },
        "date_created" : {
            "selector" : ".article-dates time[itemprop='dateCreated']"
        },
        "date_published" : {
            "selector" : ".article-dates time[itemprop='datePublished']"
        },
        "date_accepted" : {
            "selector" : ".article-dates time[data-itemprop='dateAccepted']"
        },
        "areas" : {
            "selector" : ".subject",
            "isList" : true
        },
        "keywords" : {
            "selector" : ".kwd",
            "isList" : true
        },
        "abstract" : {
            "selector" : ".abstract"
        },
        "pdf_url" : {
            "selector" : "meta[name='citation_pdf_url']",
            "attribute" : "content"
        },
        "sections" : {
            "selector" : "#article-item-main-sections > section",
            "isList" : true,
            "listSelectors" : {
                "name" : {
                    "selector" : "h2"
                },
                "text" : {
                    "selector" : "p"
                },
                "figures" : {
                    "selector" : "figure.fig",
                    "isList" : true,
                    "listSelectors" : {
                        "doi" : {
                            "selector" : ".object-id > a",
                            "attribute" : "href"
                        },
                        "src" : {
                            "selector" : ".image-container > a.fresco",
                            "attribute" : "href"
                        },
                        "caption" : {
                            "selector" : "figcaption span"
                        }
                    }
                }
            }
        }
    }
}'''

peerj_reports_selectors = '''{
    "base_url" : "https://peerj.com",
    "reports": {
        "versions" : {
            "selector" : ".publication-review-version",
            "isList" : true,
            "listSelectors" : {
                "id" : {
                    "selector" : ":self",
                    "attribute" : "id"
                },
                "editor_decision" : {
                    "selector" : ".publication-decision",
                    "listSelectors" : {
                        "status" : {
                            "selector" : ".article-recommendation"
                        },
                        "text" : {
                            "selector" : "[itemprop='reviewBody']"
                        }
                    }
                },
                "reviews" : {
                    "selector" : ".publication-review:not(.publication-decision)",
                    "isList" : true,
                    "listSelectors" : {
                        "sections" : {
                            "selector" : "h4:has(+p)",
                            "isList" : true,
                            "listSelectors" : {
                                "name" : {
                                    "selector" : ":self"
                                },
                                "text" : {
                                    "selector" : ":sibling p"
                                }
                            }
                        }
                    }
                },
                "pdf" : {
                    "selector" : ".publication-review-files a[href*='submission']",
                    "attribute" : "href"
                },
                "rebuttal" : {
                    "selector" : ".publication-review-files a[href*='rebuttal']",
                    "attribute" : "href"
                },
                "date" : {
                    "selector" : ".publication-review-submitted"
                }
            }
            
        }
    }
}'''