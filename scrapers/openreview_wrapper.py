import openreview
from openreview import tools

import os
import getpass
import datetime
import pickle

class OpenReview:
    def __init__(self, use_env_pwd=None):
        self.client = None
        self.use_env_password = use_env_pwd
        self.notes_per_venue = {}

        self.last_filename = None

    def init_client(self, username=None, passw=None):
        if passw:
            password = passw
        elif self.use_env_password:
            password = os.environ.get(self.use_env_password)
        else:
            password = getpass.getpass(prompt=f"Provide the password to {username}")

        self.client = openreview.Client(baseurl='https://api.openreview.net', username=username, password=password)

    def pull_all_venues_submissions_as_notes(self):
            venues =  list(self.client.get_group(id='venues').members)
            invitations = []
            self.notes_per_venue = {}

            invitation_types = ["Blind_Submission", "Submission", "Withdrawn_Submission", "Desk_Rejected_Submission"]
            #invitation_types = ["Withdrawn_Submission", "Desk_Rejected_Submission"]

            for venue in venues:
                for invitation_type in invitation_types:
                    invitation = f"{venue}/-/{invitation_type}"
                    invitations.append(invitation)
                    self.notes_per_venue[invitation] = list(self.client.get_all_notes(invitation=invitation, details='replies'))
            return self.notes_per_venue

    def to_pickle(self, filename=None):
        self.last_filename = filename if filename else f'{str(datetime.datetime.now())[:-7]}_notes_per_venue.pickle'
        pickle.dump(self.notes_per_venue, open(self.last_filename, 'wb'))


    def from_pickle(self, filename=None):
        chosen_file = filename if filename else self.last_filename

        if chosen_file:
            return pickle.load(open(chosen_file, 'rb'))
        else:
            return None
