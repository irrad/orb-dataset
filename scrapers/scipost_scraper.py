from __future__ import annotations

from bs4 import BeautifulSoup
from collections import namedtuple

import datetime
import pickle
import requests

BASE_URL = "https://scipost.org"

DEFAULT_SUBMISSIONS_URL = "https://scipost.org/submissions/?field=physics"
DEFAULT_FIELD = "physics"

SUBMISSIONS = "submissions"
FIELD = "?field"
SPEC = "?specialty"
PAGE = "&page"

class SciPostScraper:
    def __init__(self, parser='lxml'):
        #using lxml parser because html.parser was missing some <meta> tag closures
        self.parser = parser
        self.last_filename = None
        self.submissions_data_list = None

    def get_physics_submissions(self, chosen_url=DEFAULT_SUBMISSIONS_URL, field=DEFAULT_FIELD):

        page_numbers = self.get_pages_numbers(chosen_url)
        pages_urls_list = self.get_pages_urls_with_field(page_numbers, DEFAULT_FIELD)
        main_urls_collection = self.get_urls_from_pages(pages_urls_list)
        self.submissions_data_list = self.extract_all_submissions_versions(main_urls_collection)

        return self.submissions_data_list

    def extract_all_submissions_versions(self, main_urls_collection):
        submissions_version_list = []
        submission_number = 0
        for current_processing_url in main_urls_collection:
            submission_number = submission_number + 1
            current_submission_versions = []
            try:
                version_urls = self.extract_submission_versions_from_html(current_processing_url)
                print(f"Processing url {submission_number}/{len(main_urls_collection)}")
            except:
                print("No version links in html - continuing with single link")
                version_urls = [current_processing_url]

            for current_version in version_urls:
                #versions_articles =versions_articles +1
                try:
                    submission_info = self.extract_submission_info_from_link(current_version)
                    print(f"Retrieved and processed version: {current_version}")
                except:
                    print(f"Could not process submission info for: {current_version}")
                    continue

                try:
                    submission_reports = self.reports_from_current_version_link(current_version)
                except:
                    print(f"Could not fetch and process reports for: {current_version}")
                    submission_reports = []

                #reviews = reviews + len(submission_reports)
                current_version = {
                    'submission_info': submission_info,
                    'submission_reports': submission_reports
                }
                current_submission_versions.append(current_version)
            submissions_version_list.append(current_submission_versions)

        return submissions_version_list

    def get_pages_urls_with_field(self, page_numbers, field=DEFAULT_FIELD):
        pages_url_list = []
        for page_number in page_numbers:
            current_page_url = SciPostScraper.scipost_field_url_builder(field, page_number)
            pages_url_list.append(current_page_url)

        return pages_url_list

    def get_pages_numbers(self, chosen_url=DEFAULT_SUBMISSIONS_URL):
        main_submissions_page = requests.get(chosen_url)
        soup = BeautifulSoup(main_submissions_page.content, self.parser)

        pages = soup.find_all('a', {"class":"page"})
        no_pages = pages[-1].get_text()
        page_numbers = [n for n in range(1, int(no_pages)+1)]

        return page_numbers

    def get_urls_from_pages(self, pages_url_list):
        urls_collection = []
        for current_page_url in pages_url_list:
            print(f"Processing page {current_page_url}")
            current_page_request = requests.get(current_page_url)
            current_page_soup = BeautifulSoup(current_page_request.content, self.parser)

            html_title_class = {"class":"title"}
            current_page_titles_html = current_page_soup.find_all('h3', html_title_class)

            for title_element in current_page_titles_html:
                title_ahref = title_element.find('a')['href']
                #print(title_ahref)
                if title_ahref:
                    link = title_ahref if "http" in title_ahref else f"{BASE_URL}/{title_ahref}"
                    link = SciPostScraper.double_slash_checker(link)
                    urls_collection.append(link)

        return urls_collection

    @staticmethod
    def extract_pdf_link_from_table_paper_summary(table_element):
        pdf_link = None
        for element in table_element.find_all("a"):
            if "pdf" in element["href"] or "pdf" in str(element):
                pdf_link = element["href"]

        return pdf_link

    def extract_submission_info_from_link(self, submission_link):
        submission_page = requests.get(submission_link)
        #using lxml parser because html.parser was missing some <meta> tag closures
        soup = BeautifulSoup(submission_page.content, self.parser)

        id = soup.find('span', {'class':'breadcrumb-item'}).get_text()

        authors = []
        citation_authors_meta = soup.find_all('meta', {"name":"citation_author"})
        for element in citation_authors_meta:
            authors.append(element["content"])

        title = None
        citation_title = soup.find('meta', {"name":"citation_title"})
        title = citation_title["content"]

        # get abstract between <p> tag coming after <h3 class="mt-4">Abstract</h3>
        # Abstract is element[1], and <p> tag is accesses with next.next.next
        abstract_element = soup.find_all('h3', {"class":"mt-4"})[1]
        abstract = abstract_element.next.next.next.get_text()

        summary = soup.find_all("table", {"class": "submission summary"})
        table_paper = summary[1]
        table_keywords = summary[2]

        venue = None
        submission_date = None
        for element_tr in table_paper.find_all("tr"):
            elements_td = element_tr.find_all("td")
            if "Submitted to" in elements_td[0].get_text():
                venue = elements_td[1].get_text()
            if "Date submitted" in elements_td[0].get_text():
                submission_date = elements_td[1].get_text()

        # refactored into a separate function as SciPost pdf are hosted both on arxiv
        # and on SciPost's own space
        pdf_link = SciPostScraper.extract_pdf_link_from_table_paper_summary(table_paper)

        keywords = []
        for element in table_keywords.find_all("li"):
            keywords.append(element.get_text())

        submission_status = None
        status_element = soup.find("div", {"class":"submission status mt-2"})

        if status_element:
            submission_status_element = status_element.find("span", {"class":"label"})
            submission_status = submission_status_element.get_text()

        else:
            #if no status, then probably submitted and accepted, searching for "published" to SciPost volume
            submission_status_element = soup.find("div", {"class":"border-success"})
            submission_volume = submission_status_element.find("a")
            if submission_volume:
                submission_status = f"Published: {submission_volume.get_text()}"
                venue = submission_volume["href"]

        submission_dict = {
            'id': id,
            'title': title,
            'abstract': abstract,
            'url_link': submission_link,
            'pdf_link': pdf_link,
            'keywords': keywords,
            'submission_status': submission_status,
            'venue' : venue,
            'submission_date' : submission_date
        }

        return submission_dict

    def extract_submission_versions_from_html(self, main_submission_link):
        page = requests.get(main_submission_link)

        soup = BeautifulSoup(page.content, self.parser)

        submission_history = soup.find_all("div", {"class":"submission-contents"})
        submissions_soup = submission_history[0].find_all("div", {"class":"p-2"})


        version_urls = []
        for submission_element in submissions_soup:
            current_submission_link_tag = submission_element.find("a", {"class":"pubtitleli"})["href"]
            current_submission_link = SciPostScraper.scipost_submission_url_builder(current_submission_link_tag)

            #print(f" Retrieved version: {current_submission_link_tag}, {current_submission_link}")
            version_urls.append(current_submission_link)

        return version_urls

    def reports_from_current_version_link(self, current_submission_link):
        current_submission_page = requests.get(current_submission_link)
        current_submission_soup = BeautifulSoup(current_submission_page.content, self.parser)

        reports_soup = current_submission_soup.find_all("div", {"class":"report"})

        reports_collection = []
        for report_element in reports_soup:
            #print(report_element.find_all("h3",{"class":"highlight"}))
            # class:ps-md-4
            highlight = {"class":"highlight"}
            highlight_content = {"class":"ps-md-4"}

            report = {}
            for section_element in report_element.find_all("div",{"class":"col-12"}):
                report_section = section_element.find("h3", highlight).get_text()
                report_content = section_element.find("div", highlight_content).get_text()

                report[report_section] = report_content

            ratings = {}

            ratings = {"class":"ratings"}
            ratings_element = report_element.find_all("div", ratings)
            separate_ratings = [rating.get_text().split(": ") for rating in report_element.find_all("li")]

            rating_vals = {}
            for rating in separate_ratings:
                if len(rating) == 2:
                    rating_vals[rating[0]] = rating[1]

            review_dict = {
                'review': report,
                'ratings': rating_vals
                }


            reports_collection.append(review_dict)

        return reports_collection

    @staticmethod
    def double_slash_checker(link: str) -> str:
        return link.replace("//","/").replace(":/","://")

    @staticmethod
    def scipost_specialty_url_builder(specialty, page):
        return SciPostScraper.double_slash_checker(f"{BASE_URL}/{SUBMISSIONS}/{SPEC}={specialty}{PAGE}={page}")

    @staticmethod
    def scipost_field_url_builder(field, page):
        return SciPostScraper.double_slash_checker(f"{BASE_URL}/{SUBMISSIONS}/{FIELD}={field}{PAGE}={page}")

    @staticmethod
    def scipost_submission_url_builder(submission_link_tag):
        return SciPostScraper.double_slash_checker(f"{BASE_URL}/{submission_link_tag}")

    def to_pickle(self, filename=None):
        self.last_filename = filename if filename else f'{str(datetime.datetime.now())[:-7]}_scipost_submissions_data_list.pickle'
        pickle.dump(self.submissions_data_list, open(self.last_filename, 'wb'))

    def from_pickle(self, filename=None):
        chosen_file = filename if filename else self.last_filename

        if chosen_file:
            return pickle.load(open(chosen_file, 'rb'))
        else:
            return None
