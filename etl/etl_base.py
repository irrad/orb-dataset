from abc import ABC, abstractmethod


class EtlProcessBase(ABC):
    """Base class for ETL-based implementations that are yet to come"""

    @abstractmethod
    def extract(self):
        """Extract data from db, document, pdf."""
        pass

    @abstractmethod
    def transform(self):
        """Transform: select, parse, clean, merge/split, validate and generally
        prepare data loaded in previous step to prepare it for loading into
        selected target"""
        pass

    @abstractmethod
    def load(self):
        """Load cleaned data into memory, file or data warehouse"""
        pass
