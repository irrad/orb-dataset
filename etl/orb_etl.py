from etl.etl_base import EtlProcessBase
from scrapers.openreview_wrapper import OpenReview
from scrapers.scipost_scraper import SciPostScraper
from scrapers.peerj_scraper import PeerJScraper
from datasets.openreview import OpenreviewToOrb
from datasets.scipost import ScipostToOrb
from datasets.peerj import PeerjToOrb
from datasets.orb import OrbDataset, OrbDatasetBuilder

import dataclasses
import datetime
import json
import pickle


class OrbEtlAgent(EtlProcessBase):
    def __init__(self, scraper=None, name=None):
        self.scraper = scraper
        self.agent_id_name = name

        self.extract_results = None
        self.transform_results = None
        self.load_results = None

    def get_extract_results_filename(self):
        return f'extract_results_{self.agent_id_name}_etl_agent.pickle'


class OpenReviewEtlAgent(OrbEtlAgent):
    def __init__(self, username, env_pwd_variable):
        self.username = username
        self.use_env_pwd = env_pwd_variable
        openreview_client = OpenReview(use_env_pwd=env_pwd_variable)
        super().__init__(scraper=openreview_client, name='OpenReview')

    def extract(self):
        self.scraper.init_client(username=self.username)
        self.scraper.pull_all_venues_submissions_as_notes()

        # store data with timestamp and "latest"
        self.scraper.to_pickle()
        self.scraper.to_pickle(self.get_extract_results_filename())

        self.extract_results = self.scraper.notes_per_venue

    def transform(self, purge_submissions_without_reviews=False):
        openreview_to_orb_transformer = OpenreviewToOrb()
        notes_per_venue = self.extract_results

        flattened_source_list = []
        for key in notes_per_venue.keys():
            dict_content = notes_per_venue[key]
            flattened_source_list.extend(dict_content)

        orb_submissions = openreview_to_orb_transformer.parse_source_records(flattened_source_list)
        self.transform_results = orb_submissions

    def load(self):
        submissions_list = self.transform_results

        unique_venues_set = set()
        ratings_scope_dict = {}
        confidence_scope_dict = {}

        for submission in submissions_list:
            unique_venues_set.add(submission.venue)
            current_venue_str = submission.venue.identifier.uri
            # for OpenReview only one final version exists, counting from 1
            single_version_key = 1
            single_version = submission.article_versions[single_version_key]
            for review in single_version.reviews:
                # each review may host different ratings, so using dict pairs rating_name : OrbGrading
                for key in review.ratings.keys():
                    try:
                        ratings_scope_dict[current_venue_str][key].add(review.ratings[key])
                    except KeyError:
                        ratings_scope_dict[current_venue_str] = dict()
                        ratings_scope_dict[current_venue_str][key] = set()
                        ratings_scope_dict[current_venue_str][key].add(review.ratings[key])

                try:
                    confidence_scope_dict[current_venue_str].add(review.confidence)
                except KeyError:
                    confidence_scope_dict[current_venue_str] = set()
                    confidence_scope_dict[current_venue_str].add(review.confidence)

        for venue in unique_venues_set:
            venue_id_string = venue.identifier.uri
            venue.rating_scope = ratings_scope_dict[venue_id_string] if venue_id_string in ratings_scope_dict else None
            venue.confidence_scope = confidence_scope_dict[venue_id_string] if venue_id_string in confidence_scope_dict else None

        self.load_results = (submissions_list, list(unique_venues_set))


class SciPostEtlAgent(OrbEtlAgent):
    def __init__(self):
        scipost_scraper = SciPostScraper()
        super().__init__(scraper=scipost_scraper, name='SciPost')

    def extract(self):
        submission_tuple_list = self.scraper.get_physics_submissions()

        # store data with timestamp and "latest"
        self.scraper.to_pickle()
        self.scraper.to_pickle(self.get_extract_results_filename())

        self.extract_results = submission_tuple_list

    def transform(self, purge_submissions_without_reviews=False):
        scipost_to_orb_transformer = ScipostToOrb()
        orb_submissions = scipost_to_orb_transformer.parse_source_records(self.extract_results)

        self.transform_results = orb_submissions

    def load(self):
        submissions_list = self.transform_results

        unique_venues_set = set()
        for submission in submissions_list:
            unique_venues_set.add(submission.venue)

        self.load_results = (submissions_list, list(unique_venues_set))


class PeerjEtlAgent(OrbEtlAgent):
    def __init__(self):
        peerj_scraper = PeerJScraper()
        super().__init__(scraper=peerj_scraper, name='Peerj')

    def extract(self):
        submissions_list = self.scraper.extract_all_submissions()

        # store data with timestamp and "latest"
        self.scraper.to_pickle()
        self.scraper.to_pickle(self.get_extract_results_filename())

        self.extract_results = submissions_list

    def transform(self, purge_submissions_without_reviews=False):
        peerj_to_orb_transformer = PeerjToOrb()
        orb_submissions = peerj_to_orb_transformer.parse_source_records(self.extract_results)

        self.transform_results = orb_submissions

    def load(self):
        submissions_list = self.transform_results
        venues = set()
        for submission in submissions_list:
            venues.add(submission.venue)

        venues_list = list(venues)
        self.load_results = (submissions_list, venues_list)


class OrbEtlProcessFactory(EtlProcessBase):
    DEFAULT_PICKLE_FILE = 'EtlProcessFactory_OrbDataset.pickle'

    def __init__(self):
        self.etl_agents = []
        self.last_saved_pickle = None
        self.orb_dataset = None
        self.orb_venues = None

    def add_etl_agent(self, agent: OrbEtlAgent = None):
        if agent:
            self.etl_agents.append(agent)

    def extract(self):
        for agent in self.etl_agents:
            agent.extract()

    def transform(self, purge_submissions_without_reviews=False):
        for agent in self.etl_agents:
            agent.transform(purge_submissions_without_reviews)

    def load(self):
        orb_submissions_list = []
        orb_venues_list = []
        for agent in self.etl_agents:
            agent.load()
            submissions, venues  = agent.load_results
            orb_submissions_list.extend(submissions)
            orb_venues_list.extend(venues)

        self.orb_dataset = OrbDatasetBuilder.build_dataset(orb_submissions_list)
        self.orb_venues = orb_venues_list

    @staticmethod
    def handle_set(record):
        if isinstance(record, set):
            return list(record)
        else:
            return None

    def save_dataset_to_json_file(self, path):
        jsonFile = open(path, "w")
        orb_dataset_dict = dataclasses.asdict(self.orb_dataset)
        for chunk in json.JSONEncoder(default=OrbEtlProcessFactory.handle_set).iterencode(orb_dataset_dict):
            json.dump(chunk, jsonFile)
        jsonFile.close()

    def to_pickle(self, filename=DEFAULT_PICKLE_FILE):
        self.last_saved_pickle = filename
        pickle.dump(self.orb_dataset, open(filename, 'wb'))


    def from_pickle(self, filename=None):
        if not filename:
            filename = self.last_saved_pickle if self.last_saved_pickle else OrbEtlProcessFactory.DEFAULT_PICKLE_FILE

        self.orb_dataset = pickle.load(open(filename, 'rb'))
        return self.orb_dataset
