from __future__ import annotations

def build_xpath(node, attributes_dict):
        xpath = f"{node}"
        for attribute in attributes_dict.keys():
            xpath += f"[@{attribute}='{attributes_dict[attribute]}']"
        return xpath

def safe_key(dictionary, key, required=False):
    if required and key not in dictionary.keys():
        raise KeyError(f"<{key}> not in [{dictionary.keys()}], JSON structure incomplete - terminated")
    return dictionary[key] if key in dictionary.keys() else None

def parse_html_with_selector_config(soup, config_json, preselector=None):
    result_dict = {}
    for key, config in config_json.items():
        preselector = safe_key(config, 'preselector')
        try:
            selector = safe_key(config, 'selector', required=True)
        except KeyError as e:
            raise(e)
            
        index = safe_key(config, 'index')
        attribute = safe_key(config, 'attribute')

        if preselector:
            preselection = soup.find(preselector)
            selection = preselection.select(selector)
        elif selector == ":self":
            selection = [soup]
        elif ":sibling" in selector:
            data = selector.split(' ')
            tag = data[-1]
            selection = [soup.find_next_sibling(tag)]
        else:
            selection = soup.select(selector)

        is_list = safe_key(config, "isList")
        
        if index is None and not is_list and not isinstance(selection, list):
            index = 0
        if isinstance(index, int): # index provided as integer, 0 default for first/single element
            selection = selection[index]
            if attribute:
                result_dict[key] = selection.get(attribute)
            else:
                result_dict[key] = selection.text.strip()
                
        if isinstance(selection, list):
            list_value = []
            list_selectors = safe_key(config, 'listSelectors')

            for item in selection:
                if attribute:
                    value = item.get(attribute)
                    list_value.append(value)
                elif list_selectors:
                    value = parse_html_with_selector_config(item, list_selectors)
                    list_value.append(value)
                else:
                    value = item.text.strip()
                    list_value.append(value)
                
            if not is_list:
                if len(list_value) > 1:
                    print(f"WARNING: Key <{key}> is a list - but we expect single value! ",
                            "The results will be merged at the end of processing!")
                try:
                    list_value = "\n".join(list_value)
                except TypeError:
                    list_value = list_value[0]
                
            result_dict[key] = list_value
            
    return result_dict