import shutil

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options

from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager

    
class BrowserWrapper:
    def __init__(self, preferred_browser, wait_time=40):
        self.service = None
        self.browser_driver = self.setup_browser(preferred_browser)
        self.last_preferred_browser = preferred_browser
        self.wait_time = wait_time
    
    def setup_browser(self, preferred_browser):
        if preferred_browser == 'firefox' and BrowserWrapper.is_firefox_installed():
            options = Options()
            options.add_argument("--headless")
            options.add_argument("--no-sandbox")
            if not self.service:
                service_firefox = Service(executable_path=GeckoDriverManager().install())
                self.service = service_firefox
            driver = webdriver.Firefox(options=options, service=self.service)

        elif preferred_browser == 'chrome' and BrowserWrapper.is_chrome_installed():
            options = webdriver.ChromeOptions()
            options.add_argument('--headless')
            options.add_argument('--no-sandbox')  # Required for running in a Linux environment
            if not self.service:
                service_chrome = Service(executable_path=ChromeDriverManager().install())
                self.service = service_chrome
            driver = webdriver.Chrome(service=self.service, options=options)

        else:
            raise ValueError("No suitable browser engine was found - please make sure to install one (Firefox, Chrome, Google Chrome)")
            
        return driver

    def render_html(self, url, wait_condition=None, reset_on_finish=True):
        url = BrowserWrapper.double_slash_checker(url)
        print(f'rendering: {url}')
        html = ''
        if self.browser_driver:
            self.browser_driver.get(url)
            
            if wait_condition:
                wait = WebDriverWait(self.browser_driver, self.wait_time)
                wait.until(wait_condition)
            else:
                self.browser_driver.implicitly_wait(self.wait_time)
            
            html = self.browser_driver.page_source

        if reset_on_finish:
            self.reset_browser()
        return html

    def close_browser(self):
        if self.browser_driver:
            self.browser_driver.quit()
            self.browser_driver = None
        
    def reset_browser(self):
        self.close_browser()
        self.browser_driver = self.setup_browser(self.last_preferred_browser)
        
    @staticmethod
    def is_firefox_installed():
        return shutil.which("firefox") is not None

    @staticmethod
    def is_chrome_installed():
        return shutil.which("google-chrome") is not None or shutil.which("chromedriver") is not None
    
    @staticmethod
    def double_slash_checker(link: str) -> str:
        return link.replace("//","/").replace(":/","://")