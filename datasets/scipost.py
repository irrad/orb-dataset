from __future__ import annotations
import re

from datasets.orb import OrbSubmissionsBuilderInterface
from datasets.orb import (OrbSubmission, OrbPaper, OrbReview,
                          OrbURI, OrbVenue, OrbDecision, OrbGrading)

ranking_choices_sections = ['validity', 'significance', 'originality', 'clarity']
quality_choices_sections = ['formatting', 'grammar']

# official scipost rating constants
QUALITY_SPEC = (
    (None, '-'),
    (6, 'perfect'),
    (5, 'excellent'),
    (4, 'good'),
    (3, 'reasonable'),
    (2, 'acceptable'),
    (1, 'below threshold'),
    (0, 'mediocre'),
)

# official scipost rating constants
RANKING_CHOICES = (
    (None, '-'),
    (100, 'top'),
    (80, 'high'),
    (60, 'good'),
    (40, 'ok'),
    (20, 'low'),
    (0, 'poor')
)

# for REPORT_PUBLISH the highest status has value 1 and lowest 3, so need to revert
# to keep track of high-low values, original commented out:
# REPORT_PUBLISH_1, REPORT_PUBLISH_2, REPORT_PUBLISH_3 = 1, 2, 3

REPORT_PUBLISH_1, REPORT_PUBLISH_2, REPORT_PUBLISH_3 = 3, 2, 1
REPORT_MINOR_REV, REPORT_MAJOR_REV = -1, -2
REPORT_REJECT = -3
REPORT_ALT_JOURNAL = -4

REPORT_REC = (
    (None, '-'),
    (REPORT_PUBLISH_1, 'Publish (surpasses expectations and criteria for this Journal; among top 10%)'),
    (REPORT_PUBLISH_2, 'Publish (easily meets expectations and criteria for this Journal; among top 50%)'),
    (REPORT_PUBLISH_3, 'Publish (meets expectations and criteria for this Journal)'),
    (REPORT_MINOR_REV, 'Ask for minor revision'),
    (REPORT_MAJOR_REV, 'Ask for major revision'),
    (REPORT_ALT_JOURNAL, 'Accept in alternative Journal (see Report)'),
    (REPORT_REJECT, 'Reject')
)


class ScipostToOrb(OrbSubmissionsBuilderInterface):
    def construct_orb_submission(self, record) -> OrbSubmission:
        newest_submission = record[0]
        identifier = OrbURI(newest_submission['submission_info']['url_link'], 'URL')

        # OpenReview submissions have only one, final version available
        article_versions = ScipostToOrb.construct_article_versions(record)

        venue = ScipostToOrb.construct_orb_venue(newest_submission)

        return OrbSubmission(identifier=identifier,
                             article_versions=article_versions,
                             venue=venue)

    @staticmethod
    def construct_article_versions(submission):
        article_versions = {}
        article_versions = ScipostToOrb.parse_versions(submission)

        return article_versions

    @staticmethod
    def parse_versions(submission):
        versions = {}

        no_revisions = len(submission)
        for counter, version in enumerate(submission):
            paper = version['submission_info']
            reports = version['submission_reports']

            reviews = ScipostToOrb.parse_reports(reports)
            ordinal = no_revisions - counter

            decision_flag = True if 'publish' in paper["submission_status"].lower() else False
            decision_status = ScipostToOrb.predict_decision_from_recommendations(reviews, decision_flag)
            decision = OrbDecision(summary = paper['submission_status'], decision = decision_status)

            current_version = OrbPaper(id = ordinal,
                                       title = paper['title'],
                                       abstract = paper['abstract'],
                                       keywords = paper['keywords'],
                                       sections = None, # None for Scipost
                                       pdf = paper['pdf_link'],
                                       reviews = reviews,
                                       decision = decision,
                                       rebuttal = None, # None for Scipost
                                       accepted = decision_flag)
            versions[ordinal] = current_version
        return versions

    @staticmethod
    def parse_reports(reports):
        reviews = []

        for report in reports:
            try:
                number_id = report['id']
            except KeyError:
                number_id = 0

            recommendation_key = 'Recommendation'
            review = report['review']

            # find the Recommendation and pop it - the reference won't hold this value anymore!
            try:
                summary = review[recommendation_key]
                summary = summary.replace('\n','')
            except KeyError:
                summary = None

            review_parts = []
            for key in review.keys():
                if key != recommendation_key:
                    review_parts.append(key)
                    review_parts.append(review[key])
            review_fulltext = '\n'.join(review_parts)

            grading_dict = {}
            for key in report['ratings']:
                rating_value = ScipostToOrb.report_score_to_orb_grading(key, report['ratings'][key])
                if rating_value:
                    grading_dict[key] = rating_value

            review = OrbReview(id = number_id,
                           summary = summary,
                           review = review_fulltext,
                           ratings = grading_dict,
                           confidence = None)
            reviews.append(review)

        return reviews

    @staticmethod
    def predict_decision_from_recommendations(reviews, accepted_flag):
        no_reviews = len(reviews)
        report_points = 0
        alternative = False

        summary_existed = False
        for review in reviews:
            if review.summary:
                current_point_value = next(int(value[0]) for value in REPORT_REC if value[1]==review.summary)
                report_points += current_point_value
                summary_existed = True

        averaged_decision = None
        if summary_existed:
            average = float(report_points)/no_reviews
            int_average = int(round(average))

            if int_average == 0:
                int_average = 1 if average > 0 else -1

            averaged_decision = next(value[1] for value in REPORT_REC if value[0]==int_average)

        # try to give context if no averaged decision
        if not averaged_decision:
            status = 'accepted' if accepted_flag else 'not accepted yet'
            averaged_decision = f'AUTOMATIC EVALUATION: No formal recommendation was given, paper is {status} - please inspect summary'

        return averaged_decision

    @staticmethod
    def construct_orb_venue(record):
        SEP = '.'
        # if published there is access to proper publication year
        # and name + volume of journal/venue
        if 'publish' in record['submission_info']['submission_status'].lower():
            status = record['submission_info']['submission_status']

            # last number indicated article number in volume
            venue = record['submission_info']['venue']
            venue_id = SEP.join(venue.split(SEP)[:-1])
            regex_year = re.findall(r"\(\+?(-?\d{4})\s*\)", status)
            venue_year = int(regex_year[0]) if regex_year else None
        else:
            # it is just a journal name that it was sent to
            # and a year of submission
            venue_id = record['submission_info']['venue']
            year_str =  record['submission_info']['submission_date'].split('-')[0]
            venue_year = int(year_str)

        venue_id = venue_id.replace('/','')
        venue_publisher = 'SciPost.org'

        return OrbVenue(identifier = venue_id,
                       name = venue_id.split(SEP)[0],
                       year = venue_year,
                       publisher = venue_publisher,
                       rating_scope=ScipostToOrb.get_rating_scope(),
                       confidence_scope=None)

    @staticmethod
    def report_score_to_orb_grading(key, value):
        if key in ranking_choices_sections:
            score = next(choice[0] for choice in RANKING_CHOICES if choice[1]==value)
        elif key in quality_choices_sections:
            score = next(choice[0] for choice in QUALITY_SPEC if choice[1]==value)
        else:
            # parsing error due to old html submissions was detected few times
            # and some text was treated as key
            # print(f'Unknown rating key was found: {key}')
            return None

        return OrbGrading(score=score, description=value)

    @staticmethod
    def get_rating_scope():
        all_grading_sections = ranking_choices_sections + quality_choices_sections

        ranking_grading = set([OrbGrading(score=rank[0], description=rank[1]) for rank in RANKING_CHOICES])
        quality_grading = set([OrbGrading(score=qual[0], description=qual[1]) for qual in QUALITY_SPEC])

        rating_scope_dict = {}
        for key in all_grading_sections:
            rating_scope_dict[key] = ranking_grading if key in ranking_choices_sections else quality_grading

        return rating_scope_dict
