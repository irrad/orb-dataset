from datasets.orb import (OrbSubmission, OrbDataset)


class OrbSourceStatistics:
    def __init__(self, source_name='') -> None:
        self.source_name = source_name

        self.no_submissions = 0
        self.no_submissions_with_reviews = 0
        self.no_submissions_with_revisions = 0

        self.no_versions = 0
        self.no_versions_with_reviews = 0

        self.no_reviews = 0

    def add_submission_statistics(self, submission: OrbSubmission):
        has_revisions = False
        has_reviews = False

        no_published_submissions = 0
        no_reviews = 0
        no_versions_with_reviews = 0

        try:
            has_revisions = True if len(submission.article_versions) > 1 else False

            for key in submission.article_versions.keys():
                version = submission.article_versions[key]
                current_has_reviews = bool(len(version.reviews))
                if current_has_reviews:
                    no_versions_with_reviews += 1
                has_reviews = current_has_reviews or has_reviews
                no_reviews = no_reviews + len(version.reviews)

            self.no_submissions += 1
            self.no_submissions_with_reviews +=  int(has_reviews)
            self.no_submissions_with_revisions += int(has_revisions)

            self.no_versions += len(submission.article_versions)
            self.no_versions_with_reviews += no_versions_with_reviews

            self.no_reviews += no_reviews

        except Exception as err:
            print(f"Unexpected {err=}, {type(err)=} when handling {submission.identifier} submission statistics")
            raise(err)


class OrbStatisticsCalculator:
    def __init__(self):
        self.source = {}
        self.add_source('all')

    def add_source(self, source_name):
        self.source[source_name] = OrbSourceStatistics(source_name)

    def get_stats_string(self):
        presentation_string = ''
        presentation_string += self.source['all'].get_stats_string()

        for key in self.source.keys():
            if key == 'all':
                continue
            presentation_string += self.source[key].get_stats_string()

        return presentation_string

    def present_yourself(self):
        print(self.get_stats_string())

    def calc_statistics(self, orb_dataset: OrbDataset):
        for s in orb_dataset.submissions:
            current_source = s.venue.publisher

            if current_source not in self.source.keys():
                self.add_source(current_source)

            for source_name in ['all', current_source]:
                self.source[source_name].add_submission_statistics(s)

    def get_stats_string(self):
        stat_string = ""
        for source_name in self.source.keys():
            s = self.source[source_name]
            stat_string += \
            f"{source_name} statistics:\n" \
            f"\t- number of submissions = {s.no_submissions}, with reviews = {s.no_submissions_with_reviews},  with revisions = {s.no_submissions_with_revisions}\n" \
            f"\t- number of versions = {s.no_versions}, with reviews = {s.no_versions_with_reviews}\n" \
            f"\t- number of reviews = {s.no_reviews}\n"
        return stat_string
