**WARNING**

The actual ORB dataset is, due to storage constraints, available on Zenodo.org (click [here](https://zenodo.org/record/8053077)).

This directory contains the Python code used to manage the ORB dataset.
