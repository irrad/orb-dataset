from datasets.orb import OrbSubmissionsBuilderInterface
from datasets.orb import (OrbSubmission, OrbPaper, OrbReview,
                    OrbURI, OrbVenue, OrbDecision, OrbFigure,
                    OrbSection)


class PeerjToOrb(OrbSubmissionsBuilderInterface):
    def construct_orb_submission(self, record) -> OrbSubmission:
        paper_dict = record[0]
        
        identifier = OrbURI(paper_dict['doi'], 'DOI')
        article_versions = self.construct_article_versions(record)
        venue = PeerjToOrb.construct_orb_venue(paper_dict)

        return OrbSubmission(identifier=identifier,
                             article_versions=article_versions,
                             venue=venue)
    
    @staticmethod
    def construct_article_versions(submission):
        article_versions = list()
        article_versions = PeerjToOrb.parse_versions(submission)

        return article_versions
    
    @staticmethod
    def construct_orb_venue(record):
        venue_name = record['venue']
        venue_year = int(record['date_published'].split('-')[0])
        venue_publisher = 'PeerJ.com'
        venue_identifier = '/'.join([venue_publisher, str(venue_year), venue_name.replace(' ', '-').lower()])


        return OrbVenue(identifier=OrbURI(venue_identifier, 'string'), name=venue_name, year=venue_year,
                        publisher=venue_publisher, rating_scope=None, confidence_scope=None)
    
    @staticmethod
    def parse_versions(submission):
        paper_dict = submission[0]
        reports_dict = submission[1]

        # metadate of published version
        title = paper_dict['title']
        abstract = paper_dict['abstract']
        keywords = paper_dict['keywords'] #there is also paper['areas'] list that can be usefull'
        sections = PeerjToOrb.parse_sections(paper_dict['sections'])
        official_pdf = OrbURI(paper_dict['pdf_url'], 'URL')

        versions = {}
        rebuttals = {}

        try:
            reports_per_version = reports_dict['versions']
        except TypeError:
            # if peer review is not public, the versions are not available
            return {}

        for report in reports_per_version:
            paper = PeerjToOrb.parse_report_to_paper(report)
            paper.title = title
            paper.keywords = keywords

            versions[paper.id] = paper

            if report['rebuttal']:
                if 'peerj.com' not in report['rebuttal']:
                    report['rebuttal'] = f"https://peerj.com{report['rebuttal']}"

                # rebuttal linked in current version always refers to previous version
                rebuttal_to_version = paper.id - 1
                rebuttals[rebuttal_to_version] = OrbURI(report['rebuttal'], 'URL')


        if 0 in rebuttals.keys():
            # (1) in the new submissions, the rebuttal to (n) is attached to version (n+1)
            # (2) but in first peerj reports the rebuttal (n) was attached to proper version (n)
            # during parsing above, the default was (1) so key=0 indicates needed fix as below
            # (the  version numbering starts always from 1)
            fixed_rebuttals =  {key+1 : value for key, value in rebuttals.items()}
            rebuttals = fixed_rebuttals

        for key in rebuttals.keys():
            versions[key].rebuttal = rebuttals[key]

        # for official version we assign all the metadata
        official_idx = max(versions.keys())
        versions[official_idx].abstract = abstract
        versions[official_idx].sections = sections
        versions[official_idx].pdf = official_pdf

        return versions
    
    @staticmethod
    def parse_report_to_paper(report):
        ordinal = int(report['id'].lower().replace('version-0-', ''))

        if isinstance(report['editor_decision'], dict):
            decision_status = report['editor_decision']['status']
            decision_text = report['editor_decision']['text']
        else:
            # early version of peerj submissions do not have separate
            # editor decission, usually they are accepted
            decision_status = 'unknown'
            decision_text = ''
            print('unknown_decision')

        decision = OrbDecision(summary = decision_text,
                                decision = decision_status)

        accepted_flag = True if 'accept' in decision_status else False

        pdf = OrbURI(report['pdf'], 'URL')

        reviews = []
        reviews_dict = report['reviews']
        for raw_review in reviews_dict:
            review = PeerjToOrb.parse_review(raw_review)
            reviews.append(review)

        paper = OrbPaper(id=ordinal,
                    title=None,
                    abstract=None,
                    keywords=None, #there is also paper['areas'] list that can be usefull']
                    sections=None,
                    pdf=pdf,
                    reviews=reviews,
                    decision=decision,
                    rebuttal=None,
                    accepted=accepted_flag)

        return paper

    @staticmethod
    def parse_review(review):
        review_parts = []
        for section in review['sections']:
            section_name = section['name']
            section_text = section['text']

            review_parts.append(section_name)
            review_parts.append(section_text)

        review_fulltext = '\n'.join(review_parts)

        review = OrbReview(id = 0,
                           summary = None,
                           review = review_fulltext,
                           ratings = None,
                           confidence = None)
        return review

    @staticmethod
    def parse_figures(figures):
        section_figures = list()
        for figure in figures:
            doi = figure['doi']
            src = figure['src']
            caption = figure['caption']
            fig = OrbFigure(caption=caption, source=OrbURI(uri=src, type='URL'))
            section_figures.append(fig)
        return section_figures

    @staticmethod
    def parse_sections(sections):
        paper_sections = list()
        figures = None
        num = 0
        for section in sections:
                name = section['name']
                text = section['text']
                figures_dict = section['figures']
                figures = PeerjToOrb.parse_figures(figures_dict)

                section = OrbSection(id=num, header=name, text=text, figures=figures)
                paper_sections.append(section)
                num += 1

        return paper_sections