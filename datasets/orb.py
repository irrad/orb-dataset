from __future__ import annotations
from abc import ABC, ABCMeta, abstractmethod
from dataclasses import dataclass, field


@dataclass(eq=True, frozen=True)
class OrbGrading:
    score: int
    description: str


@dataclass(unsafe_hash=True)
class OrbVenue:
    identifier: OrbURI = field(hash=True)
    name: str = field(hash=True)
    year: int = field(hash=True)
    publisher: str = field(hash=True)
    rating_scope: dict[str, set[OrbGrading]] = field(hash=False)
    confidence_scope: list[OrbGrading] = field(hash=False)


@dataclass
class OrbReview:
    id: str
    summary: str
    review: str
    ratings: dict[str, OrbGrading]
    confidence: OrbGrading


@dataclass(unsafe_hash=True)
class OrbURI:
    uri: str = field(hash=True)
    type: str = None


@dataclass
class OrbDecision:
    summary: str
    decision: str

@dataclass
class OrbRebuttal:
    letter: OrbURI
    response: str


@dataclass
class OrbFigure:
    caption: str
    source: OrbURI


@dataclass
class OrbSection:
    id: int
    header: str
    text: str
    figures : list[OrbFigure]


@dataclass
class OrbPaper:
    id: int
    title: str
    abstract: str
    keywords: list[str]
    sections: list[OrbSection]
    pdf: OrbURI
    reviews: list[OrbReview]
    decision: OrbDecision
    rebuttal: OrbRebuttal
    accepted: bool


@dataclass
class OrbSubmission:
    identifier: OrbURI
    article_versions: dict[int,OrbPaper]
    venue: OrbVenue = None


@dataclass
class OrbDataset:
    submissions: list[OrbSubmission]
    VoID: OrbURI
    ontology: OrbURI
    zenodo: OrbURI


class OrbSubmissionsBuilderInterface(metaclass=ABCMeta):
    def parse_source_records(self, source_data: list) -> list[OrbSubmission]:
        submissions = list()
        
        for record in source_data:
            submissions.append(self.construct_orb_submission(record))

        return submissions
    
    @abstractmethod
    def construct_orb_submission(self, record) -> OrbSubmission:
        """Abstract method to implement conversion from raw record to OrbSubmission"""
        raise NotImplementedError


class OrbDatasetBuilder:
    REPO_URL_FILES = "https://gitlab.cern.ch/irrad/orb-dataset/-/tree/master/"
    METADATA_URL =  REPO_URL_FILES + 'metadata/'
    ZENODO_DOI = "https://zenodo.org/doi/10.5281/zenodo.8053076"

    @staticmethod
    def build_dataset(submissions):
        void_url = OrbDatasetBuilder.METADATA_URL + 'orb_void.ttl'
        ontology_url = OrbDatasetBuilder.METADATA_URL + 'orb.rdf'

        dataset = OrbDataset(submissions=submissions, VoID=void_url, ontology=ontology_url,
                             zenodo=OrbDatasetBuilder.ZENODO_DOI)
        return dataset
