from __future__ import annotations
import re

from datasets.orb import OrbSubmissionsBuilderInterface
from datasets.orb import (OrbSubmission, OrbPaper, OrbReview,
                    OrbURI, OrbVenue, OrbDecision, OrbGrading)


class OpenreviewToOrb(OrbSubmissionsBuilderInterface):
    def construct_orb_submission(self, record) -> OrbSubmission:
        note_as_dict = record.__dict__
        
        submission_id_url = f"https://openreview.net/forum?id={note_as_dict['id']}"
        identifier = OrbURI(submission_id_url, 'URL')

        # OpenReview submissions have only one, final version available
        final_article_version = self.construct_final_version(note_as_dict)
        
        venue = OpenreviewToOrb.construct_orb_venue(note_as_dict)

        return OrbSubmission(identifier=identifier,
                             article_versions={1: final_article_version},
                             venue=venue)
    
    @staticmethod
    def construct_final_version(note):
        content  = note['content']
        
        # metadate of published version
        ordinal = 1
        title = content['title'] if 'title' in content else None
        abstract = content['abstract'] if 'abstract' in content else None
        keywords = content['keywords'] if 'keywords' in content else None
        sections = None # do not exist in OpenReview data
        try:
            official_pdf = OrbURI(f"https://openreview.net/{content['pdf']}", 'URL')
        except KeyError:
            # sometimes the pdf does not exist, eg for tracks like "Blog Track"
            official_pdf = None

        decision_str, reviews = OpenreviewToOrb.construct_reviews_from_note(note)
        decision_flag = None
        if decision_str:
            decision_flag = OpenreviewToOrb.predict_decision_flag(decision_str)

        rebuttal = None # does not exist in OpenReview data

        final_version = OrbPaper(id=ordinal,
                                 title=title,
                                 abstract=abstract,
                                 keywords=keywords,
                                 sections=None,
                                 pdf=official_pdf,
                                 reviews=reviews,
                                 decision=OrbDecision(summary=None, decision=decision_str),
                                 rebuttal=None,
                                 accepted=decision_flag)
        
        return final_version
    
    @staticmethod
    def construct_reviews_from_note(note):
        replies = None
        if 'replies' in note['details']:
            replies = note['details']['replies']
        elif 'directReplies' in note['details']:
            replies = note['details']['directReplies']
        else:
            print(f"No replies found, listing details' keys: {note['details'].keys()}, note's full details {note}")

        decision = None
        decision_reply_found = False
        reviews = []
        for reply in replies:
            if reply['invitation'].endswith('Decision'):
                decision = reply['content']['decision'] if 'decision' in reply['content'].keys() else None
                decision_reply_found = True
            if reply['invitation'].endswith('Meta_Review') and decision_reply_found is False:
                decision = reply['content']['recommendation'] if 'recommendation' in reply['content'].keys() else None
            if reply['invitation'].endswith('Official_Review'):
                review = OpenreviewToOrb.construct_review_from_reply(reply)
                reviews.append(review)

        return decision, reviews

    @staticmethod
    def construct_review_from_reply(reply):
        id_key = reply['id']
        content = reply['content']

        review_section_keys, grading_section_keys = OpenreviewToOrb.identify_reply_section_keys(reply)
        review_fulltext = OpenreviewToOrb.create_review_fulltext(reply, review_section_keys)

        presumed_rating_keys = ['preliminary_rating', 'final_rating', 'final_rating_after_the_rebuttal', 'rating', 'overall_rating',
                                'recommendation', 'evaluation', 'review_rating', 'score', 'Overall score', 'Overall Score',
                                'Q6 Overall score']
        presumed_confidence_keys = ['confidence', 'Confidence', 'review_confidence', 'Q8 Confidence in your score']
        presumed_summary_keys = ['title', 'summary', 'summary_of_the_paper', 'summary_of_the_review', 'summary of review',
                                 'summary and contributions', 'Brief summary', 'Summary', 'summary_of_contributions',
                                 'Q1 Summary and contributions']

        main_rating = None
        confidence = None
        summary = None

        actual_rating_key = OpenreviewToOrb.find_actual_key_from_presumed_keys(presumed_rating_keys, grading_section_keys)
        if actual_rating_key:
            main_rating = content[actual_rating_key]

        actual_confidence_key = OpenreviewToOrb.find_actual_key_from_presumed_keys(presumed_confidence_keys, grading_section_keys)
        if actual_confidence_key:
            confidence = content[actual_confidence_key]

        actual_summary_key = OpenreviewToOrb.find_actual_key_from_presumed_keys(presumed_summary_keys, review_section_keys)
        if actual_summary_key:
            summary = content[actual_summary_key]

        final_orb_grading = OpenreviewToOrb.orb_grading_from_comment(main_rating)
        ratings = {}
        ratings['final'] = final_orb_grading
        
        confidence_orb_grading = OpenreviewToOrb.orb_grading_from_comment(confidence)

        return OrbReview(id=id_key, summary=summary, review=review_fulltext, ratings=ratings, confidence=confidence_orb_grading)

    @staticmethod
    def identify_grading_section_keys(reply_content: dict):
        grading_section_keys = []

        for key in reply_content.keys():
            match = re.search(r'^[-]*[0-9]+:[\s]*.*', str(reply_content[key]))
            if match:
                grading_section_keys.append(key)

        return grading_section_keys

    @staticmethod
    def identify_review_section_keys(reply_content: dict):
        review_section_keys = []

        # grading keys are easier to identify
        non_grading_keys = [key for key in reply_content if key not in OpenreviewToOrb.identify_grading_section_keys(reply_content)]

        for key in non_grading_keys:
            # naive assumption that any remaining dict entry containing string is a review section
            if isinstance(reply_content[key], str) and len(reply_content[key]) > 20:
                review_section_keys.append(key)

        return review_section_keys

    @staticmethod
    def identify_reply_section_keys(reply: dict):
        reply_content = reply['content']

        review_section_keys = OpenreviewToOrb.identify_review_section_keys(reply_content)
        grading_section_keys = OpenreviewToOrb.identify_grading_section_keys(reply_content)

        return review_section_keys, grading_section_keys

    @staticmethod
    def create_review_fulltext(reply: dict, review_keys: list) -> str:
        reply_content = reply['content']

        review_section = {}
        review_fulltext = ""
        for key in review_keys:
            review_section[key] = reply_content[key] if reply_content[key] else ""
            review_fulltext = review_fulltext + f"[{key}]: {review_section[key]} \n"

        return review_fulltext

    @staticmethod
    def find_actual_key_from_presumed_keys(presumed_keys: list[str], target_dict_keys: list[str]) -> str:
        actual_key = None
        for key in presumed_keys:
            if key in target_dict_keys:
                actual_key = key

        return actual_key

    @staticmethod
    def orb_grading_from_comment(comment=None) -> OrbGrading:
        if not comment or comment == 'None':
            return None

        # separator is a colon with a space
        separator = ": "
        items = comment.split(separator)

        # sometimes the colon happens in the text, resulting in more than two elements
        # (first is always grade after initial regex search in notes)
        if separator in comment and len(items) >= 2:
            return OrbGrading(int(items[0]), " ".join(items[1:]))
        else:
            return OrbGrading(None, items[0])


    @staticmethod
    def predict_decision_flag(decision):
        accept_words = ['accept', 'invite', 'above', '7: Good paper']
        reject_words = ['reject', 'below']

        decision_flag = None
        for word in accept_words:
            if word in decision.lower():
                decision_flag = True

        # sometimes 'accept' is also in <not accepted>, <below acceptance>
        for word in reject_words:
            if word in decision.lower():
                decision_flag = False

        return decision_flag
    
    @staticmethod
    def construct_orb_venue(note):
        venue_string_ = note['invitation']

        name = venue_string_.split('-')[0]
        year = OpenreviewToOrb.extract_year_from_venue_string(venue_string_)
        
        venue_name = venue_string_.split('-')[0]
        venue_year = OpenreviewToOrb.extract_year_from_venue_string(venue_string_)
        venue_publisher = 'OpenReview.net'
        venue_identifier = '/'.join([venue_publisher, venue_name.replace(' ', '-').lower()])


        return OrbVenue(identifier=OrbURI(venue_identifier, 'string'), name=venue_name, year=venue_year,
                        publisher=venue_publisher, rating_scope=None, confidence_scope=None)

    @staticmethod
    def extract_year_from_venue_string(venue_string_):
        year_items = re.findall(r'\d\d\d\d', venue_string_)
        if len(year_items)==1:
            return int(year_items[0])
        else:
            # if more numbers matching regex appear, it was established that the first one was the year
            # but sometimes there is also no such data as year available
            # print(f"*exception encountered for {venue_string_}: {year_items}")
            if len(year_items)==0:
                return None
            else:
                return int(year_items[0])
