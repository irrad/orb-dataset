# The Open Review-Based Dataset Project

Original publication date: August 28, 2023\
Latest revision date: September, 02, 2024

ORB Dataset\
ver. 1.2.0
-

The Open Review–Based dataset (ORB) includes a curated list of more than 62,000 scientific papers
with their 157,000 reviews and final decisions. An arXiv preprint describing ORB can be found as "The Open Review-Based (ORB) dataset: Towards Automatic Assessment of Scientific Papers and Experiment Proposals in High-Energy Physics", Jaroslaw Szumega, Lamine Bougueroua, Blerina Gkotse, Pierre Jouvelot and Federico Ravotti, [here](https://arxiv.org/abs/2312.04576).  

This
data is gathered from three sources: the [OpenReview.net](https://openreview.net), [PeerJ.com](https://peerj.com) and [SciPost.org](https://scipost.org) websites. However, given the rapidly evolving nature of the Open Science movement, the ORB software infrastructure that supplements the ORB dataset is designed to accommodate additional resources in the future.

This ORB repository includes
*  Python code (interfaces
and implementations) to translate document data and metadata into a structured
and high-level representation, 
*  an ETL process (Extract, Transform, Load) to
facilitate the automatic updates from defined sources,
* [data files](https://zenodo.org/record/8205645) representing
the structured data (note that, due to storage constraints, these files are stored on Zenodo.org),
* the orb2.rdf ontology and
* the orb_void.ttl dataset description following the Vocabulary of Interlinked Datasets (VoID).

ORB statistics
-

|Source        |Unique submissions|Submissions with revisions |Submissions with reviews|Number of reviews|
|--------------|-----------------:|--------------------------:|-----------------------:|----------------:|
|OpenReview.net|            39,577|                        ---|                  27,510|           95,734|
|Scipost.org   |             3,313|                      2,391|                   3,210|            8,715|
|PeerJ         |            20,048|                     13,195|                  13,218|           52,633|
|              |                  |                           |                        |                 |
|**Total**     |            62,938|                     15,586|                  43,938|          157,082|

ORB Ontology
-

orb2.rdf is an OWL-based ontology for the Open Review-Based dataset.

Protégé was used to define orb2.rdf.

Keywords
-

Ontology, OWL, irradiation experiment, data management, High Energy Physics.

Distribution and Licensing
-
The ORB dataset is built upon available services and data: OpenReview (open access, MIT License specified for API client) and SciPost (open access, Creative Commons Attribution 4.0 International (CC BY 4.0) License).

The ORB dataset is also licensed under the Creative Commons Attribution 4.0 International (CC BY 4.0) License. The copyright remains the authors' property. However, the authors agree to sharing and adaptation according to specified license terms.
This policy was adopted so as to facilitate the usage of the ORB dataset, its relevant code and other assets.


Maintenance
-
Periodical dataset updates are planned, and the ORB maintenance is scheduled as follows
* Updates: rerunning ETL processes to download, process and store the new submissions that arrived since the previous datafiles publication,
* Maintenance: necessary bug fixing and new features, including the introduction of new datasources and relevant code implementation.

Additionally, the code is publicly available on CERN Gitlab and licensed under Creative Commons Attribution 4.0 International (CC BY 4.0) License. Therefore everyone willing has access to accommodate the dataset to their use and perform updates.

Setting up additional repository to coordinate git pull requests is planned. This README.md will be updated then accordingly.


Contributors
-
CERN, Geneva, Switzerland

Mines Paris, PSL University, Paris, France

EFREI, Villejuif, France


<!---

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.cern.ch/irrad/orb-dataset.git
git branch -M master
git push -uf origin master
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.cern.ch/irrad/orb-dataset/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
