import sys
sys.path.append("..")

from etl.orb_etl import OrbEtlProcessFactory, SciPostEtlAgent, OpenReviewEtlAgent, PeerjEtlAgent
from datasets.orb import OrbDataset

user = "jaroslaw.szumega@cern.ch"
env_pasword_var = "ORB_PWD"
openreview_etl = OpenReviewEtlAgent(user, env_pasword_var)

scipost_etl = SciPostEtlAgent()

peerj_etl = PeerjEtlAgent()

orb_etl_factory = OrbEtlProcessFactory()

orb_etl_factory.add_etl_agent(openreview_etl)
orb_etl_factory.add_etl_agent(scipost_etl)
orb_etl_factory.add_etl_agent(peerj_etl)

purge_submissions_without_reviews = False

orb_etl_factory.extract()
orb_etl_factory.transform(purge_submissions_without_reviews)
orb_etl_factory.load()

orb_dataset = orb_etl_factory.orb_dataset

orb_etl_factory.save_dataset_to_json_file("ETL_OrbDataset.json")
