import sys
sys.path.append("..")

from etl.orb_etl import OrbEtlProcessFactory, SciPostEtlAgent, OpenReviewEtlAgent, PeerjEtlAgent
from datasets.orb import OrbDataset
from datasets.orb_utils import OrbStatisticsCalculator
import datetime

user = "jaroslaw.szumega@cern.ch"
env_pasword_var = "ORB_PWD"

# skipping extract step and assigning pickle data as extract_results
openreview_etl = OpenReviewEtlAgent(user, env_pasword_var)
openreview_extract_filename = openreview_etl.get_extract_results_filename()
openreview_etl.extract_results = openreview_etl.scraper.from_pickle(openreview_extract_filename)

scipost_etl = SciPostEtlAgent()
scipost_extract_filename = scipost_etl.get_extract_results_filename()
scipost_etl.extract_results = scipost_etl.scraper.from_pickle(scipost_extract_filename)

peerj_etl = PeerjEtlAgent()
peerj_extract_filename = peerj_etl.get_extract_results_filename()
peerj_etl.extract_results = peerj_etl.scraper.from_pickle(peerj_extract_filename)

purge_submissions_without_reviews = False

orb_etl_factory = OrbEtlProcessFactory()
orb_etl_factory.add_etl_agent(openreview_etl)
orb_etl_factory.add_etl_agent(scipost_etl)
orb_etl_factory.add_etl_agent(peerj_etl)

orb_etl_factory.transform(purge_submissions_without_reviews)
orb_etl_factory.load()

orb_dataset = orb_etl_factory.orb_dataset

orb_etl_factory.save_dataset_to_json_file("ETL_OrbDataset.json")

today_date = str(datetime.datetime.now())[:-7]
filename_now = f'{today_date}_EtlProcessFactory_OrbDataset.pickle'
print(filename_now)
orb_etl_factory.to_pickle(filename_now)
orb_etl_factory.save_dataset_to_json_file(f"ETL_OrbDataset_{today_date}.json")
orb_dataset = orb_etl_factory.from_pickle()

stats = OrbStatisticsCalculator()
stats.calc_statistics(orb_dataset)
stats.present_yourself()
